create table branch(
    id int primary key auto_increment,
    client_Id int,
    name varchar(128),
    foreign key (client_Id) references client(id)
);

create table contract(
    id int primary key auto_increment,
    branchId int,
    name varchar(128),
    foreign key (branchId) references branch(id)
);