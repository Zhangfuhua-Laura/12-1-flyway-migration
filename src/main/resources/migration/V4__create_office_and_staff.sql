create table office(
    id int primary key auto_increment,
    country varchar(64),
    city varchar(64)
);

create table staff(
    id int primary key auto_increment,
    officeId int,
    firstName varchar(64),
    lastName varchar(64),
    foreign key (officeId) references office(id)
);