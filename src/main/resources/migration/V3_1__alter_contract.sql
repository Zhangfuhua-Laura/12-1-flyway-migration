alter table contract add serviceId int;
alter table contract add constraint serviceId foreign key (serviceId) references service(id);