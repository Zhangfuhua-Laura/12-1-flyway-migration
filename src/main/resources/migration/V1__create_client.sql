create table client(
    id int primary key auto_increment,
    full_name varchar(128) not null,
    abbreviation varchar(6) not null
);