create table contractServiceOffice(
    officeId int,
    contractId int,
    foreign key (officeId) references office(id),
    foreign key (contractId) references contract(id),
    primary key (officeId, contractId)
);