alter table contractServiceOffice add staffId int;
alter table contractServiceOffice add constraint staffId foreign key (staffId) references staff(Id);